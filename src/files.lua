function files_exists(filePath)
    if filePath == nil or type(filePath) ~= 'string' or filePath == ''
    then
        return false
    end

    -- attempt to open the file (will fail if file doesn't exist)
    local wasFileFound = false
    local fileHandler = io.open(filePath, "r")

    if fileHandler
    then
        wasFileFound = true
        fileHandler:close()
    end

    return wasFileFound
end

function files_strToFile(str, filePath)
    local fileHandler = io.open(filePath, "w")
    if fileHandler
    then
        fileHandler:write(str)
        fileHandler:close()
    end
end

function files_fileToStr(filePath)
    local str
    local fileHandler = io.open(filePath, "rb")
    if fileHandler
    then
        str = fileHandler:read("*all")
        fileHandler:close()
    end
    return str
end

function files_loadLines(filePath)
    local lines = {}
    local fileHandler = io.open(filePath, "r")
    if fileHandler
    then
        for line in fileHandler:lines()
        do
            table.insert(lines, line)
        end
        fileHandler:close()
    end
    return lines
end

function files_tsvToArray(filePath)
    -- load all file lines as tsv (Tabulation Separated Values)
    local tsvEntries = {}
    local fileHandler = io.open(filePath, "r")
    if fileHandler
    then
        for line in fileHandler:lines()
        do
            local currentLineTsv = split(line, '\t')
            if type(currentLineTsv) == 'table' and #currentLineTsv > 0
            then
                table.insert(tsvEntries, currentLineTsv)
            end
        end
        fileHandler:close()
    end
    return tsvEntries
end

function files_txtHexToByteArray(filePath)
    -- load all file lines as tsv (Tabulation Separated Values)
    local bytesArray = ''
    local fileHandler = io.open(filePath, "r")
    if fileHandler
    then
        for line in fileHandler:lines()
        do

            line = line:lower()
            local pat = "([0-9a-f][0-9a-f])"
            local last_end = 1
            local s, e, cap = line:find(pat, 1)
            while s do
                if s ~= 1 or cap ~= "" then
                    local b = string.char(tonumber(cap,16))
                    bytesArray = bytesArray .. b
                end
                last_end = e+1
                s, e, cap = line:find(pat, last_end)
            end
        end
        fileHandler:close()
    end
    return bytesArray
end

-- NOT MY CODE (http://lua-users.org/wiki/SplitJoin) (improved with 3rd param)
function split(str, pat, keepEmptyEntries)
   if keepEmptyEntries ~= true then keepEmptyEntries = false end
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or (cap ~= "" or (keepEmptyEntries == true)) then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end
