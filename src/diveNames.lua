diveNumberToDiveName =
{
    ['001A'] = 'Chute avant droite',
    ['001B'] = 'Entrée à l’eau debout carpé',
    ['001C'] = 'Entrée à l’eau assis groupé',

    ['100A'] = 'Chandelle avant',

    ['101A'] = 'Plongeon avant tendu',
    ['101B'] = 'Plongeon avant carpé',
    ['101C'] = 'Plongeon avant groupé',

    ['102B'] = 'Saut périlleux avant carpé',
    ['102C'] = 'Saut périlleux avant groupé',
    ['103B'] = 'Saut pér. et demi avant carpé',
    ['103C'] = 'Saut pér. et demi avant groupé',
    ['104B'] = 'Double saut périlleux avant carpé',
    ['104C'] = 'Double saut périlleux avant groupé',
    ['105B'] = 'Double saut pér. et demi avant carpé',
    ['105C'] = 'Double saut pér. et demi avant groupé',


    ['002A'] = 'Chute arrière droite',
    ['002B'] = 'Entrée à l’eau arrière carpé',
    ['002C'] = 'Entrée à l’eau arrière groupé',

    ['200A'] = 'Chandelle arrière',

    ['201A'] = 'Plongeon arrière tendu',
    ['201B'] = 'Plongeon arrière carpé',
    ['201C'] = 'Plongeon arrière groupé',
    ['202B'] = 'Saut périlleux arrière carpé',
    ['202C'] = 'Saut périlleux arrière groupé',
    ['203B'] = 'Saut pér. et demi arrière carpé',
    ['203C'] = 'Saut pér. et demi arrière groupé',
    ['204B'] = 'Double saut périlleux arrière carpé',
    ['204C'] = 'Double saut périlleux arrière groupé',
    ['205B'] = 'Double saut pér. et demi arrière carpé',
    ['205C'] = 'Double saut pér. et demi arrière groupé',


    ['301A'] = 'Plongeon renversé tendu',
    ['301B'] = 'Plongeon renversé carpé',
    ['301C'] = 'Plongeon renversé groupé',
    ['302B'] = 'Saut périlleux renversé carpé',
    ['302C'] = 'Saut périlleux renversé groupé',
    ['303B'] = 'Saut pér. et demi renversé carpé',
    ['303C'] = 'Saut pér. et demi renversé groupé',
    ['304B'] = 'Double saut périlleux renversé carpé',
    ['304C'] = 'Double saut périlleux renversé groupé',
    ['305B'] = 'Double saut pér. et demi renversé carpé',
    ['305C'] = 'Double saut pér. et demi renversé groupé',


    ['401A'] = 'Plongeon retourné tendu',
    ['401B'] = 'Plongeon retourné carpé',
    ['401C'] = 'Plongeon retourné groupé',
    ['402B'] = 'Saut périlleux retourné carpé',
    ['402C'] = 'Saut périlleux retourné groupé',
    ['403B'] = 'Saut pér. et demi retourné carpé',
    ['403C'] = 'Saut pér. et demi retourné groupé',
    ['404B'] = 'Double saut périlleux retourné carpé',
    ['404C'] = 'Double saut périlleux retourné groupé',
    ['405B'] = 'Double saut pér. et demi retourné carpé',
    ['405C'] = 'Double saut pér. et demi retourné groupé',


    ['610A'] = 'Plongeon en équilibre avant droit',
    ['620A'] = 'Plongeon en équilibre arrière droit',

    ['611A'] = 'Équilibre demi saut pér. avant tendu',
    ['621A'] = 'Équilibre demi saut pér. arrière tendu',

    ['612A'] = 'Équilibre saut pér. avant tendu',
    ['612B'] = 'Équilibre saut pér. avant carpé',
    ['612C'] = 'Équilibre saut pér. avant groupé',

    ['622A'] = 'Équilibre saut pér. arrière tendu',
    ['622B'] = 'Équilibre saut pér. arrière carpé',
    ['622C'] = 'Équilibre saut pér. arrière groupé',

    ['5132D'] = 'Diabolique avant'
}