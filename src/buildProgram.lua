#!/usr/bin/lua

require 'log'
require 'files'
require 'diveProgram'

-- command line arguments
local poolLayoutFilePath
local diveListFilePath
local galaNameStr
local galaVersionStr
local htmlFileName
local htmlAutoRefreshPeriod
if #arg < 2
then
    print('usage: ' .. arg[0] .. ' poolLayout.tsv diveList.tsv [galaName] [galaVersion] [htmlOutputFileName] [htmlAutoRefreshPeriod]')
    os.exit(-1)
else
    poolLayoutFilePath = arg[1]
    diveListFilePath = arg[2]
    -- Optional params
    galaNameStr = arg[3] or 'Gala'
    galaVersionStr = arg[4] or '1'
    htmlFileName = arg[5] or 'programme.html'
    htmlAutoRefreshPeriod = arg[6] or nil
end

-- Check source file exist
if files_exists(poolLayoutFilePath) ~= true
then
    log(1, 'File doesn\'t exist: '.. (poolLayoutFilePath or ''))
    os.exit(-1)
end
if files_exists(diveListFilePath) ~= true
then
    log(1, 'File doesn\'t exist: '.. (diveListFilePath or ''))
    os.exit(-1)
end

-- Load pool layout
log(2, 'Load pool layout:', poolLayoutFilePath)
if diveProgram_loadPool(poolLayoutFilePath) ~= true then
    os.exit(-2)
end
diveProgram_printPool()

-- Load dives
log(2, 'Load dive list:', diveListFilePath)
if diveProgram_loadDives(diveListFilePath) ~= true then
    os.exit(-2)
end
diveProgram_setGalaName(galaNameStr)
diveProgram_setVersionNumber(galaVersionStr)
diveProgram_setAutoRefreshPeriod(htmlAutoRefreshPeriod)

diveProgram_printDives()

diveProgram_saveProgramHtml(htmlFileName)
