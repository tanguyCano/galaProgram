-- Helper for printing feedback

function log_PrintTable(ta, label)
    if label ~= nil then print(label) end
    for i=1,#ta
    do
        print(i,ta[i])
    end
end

function log_Print2dTable(ta, label)
    if label ~= nil then print(label) end
    local i,j
    for i=1,#ta
    do
        print(table.concat((ta[i] or {}), ','))
    end
end

local maxLogLevel = 4
function log(lvl, ...)
    if lvl <= maxLogLevel
    then
        print(...)
    end
end

function log_byteArrayToString(b)
    local str = ''
    local i
    for i=1,string.len(b)
    do
        str = str .. string.format('%02x ', string.byte(b, i))
    end
    return str
end
function log_byteArray(b)
    print(log_byteArrayToString(b))
end

function log_byteArray_CStyle(b)
    local str = ''
    local i
    for i=1,string.len(b)
    do
        str = str .. string.format('0x%02x, ', string.byte(b, i))
    end
    print(str)
end
