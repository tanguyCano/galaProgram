-- Generate dive programs from a gala

require 'log'
require 'files'
require 'diveNames'

-- #### General configuration function ####
local genDateStr = os.date('%c') or ''
local galaName = 'Gala'
local version =  '('.. genDateStr .. ')'
local autoRefreshPeriod = nil

function diveProgram_setGalaName(name)
    if type(name) == 'string' then
        galaName = name
    else
        galaName = ''
    end
end

function diveProgram_setVersionNumber(num)
    if type(num) ~= 'number' and type(num) ~= 'string' then
        num = 'nil'
    end
    version = '(v' .. num .. ' ' .. (genDateStr or 'nil') .. ')'
end

function diveProgram_setAutoRefreshPeriod(period)
    if type(period) == 'number' or type(period) == 'string' then
        autoRefreshPeriod = period
    else
        autoRefreshPeriod = nil -- Disabled
    end
end

-- #### Pool layout function ####

local boardArray -- array of {columnIndex, name, bgColor}
local boardNameLookup -- boardArray[i].name -> boardArray[i]
function diveProgram_printPool()
    if boardArray == nil then
        log(2, 'No pool layout loaded')
        return false
    end

    for _,boardItem in ipairs(boardArray) do
        local colorStr
        if boardItem.bgColor ~= nil then
            colorStr = string.format('0x%06x', boardItem.bgColor)
        end
        print(boardItem.columnIndex, boardItem.name, colorStr)
    end

    return true
end

function diveProgram_loadPool(poolLayoutFilePath)
    -- Clear previous
    boardArray = nil
    boardNameLookup = nil

    -- Load layout tsv
    local layoutTsvArray = files_tsvToArray(poolLayoutFilePath)
    if layoutTsvArray == nil then
        log(2, 'Unable to load pool layout', poolLayoutFilePath)
        return false
    end

    -- Parse tsv
    for lineNumber, lineTsv in ipairs(layoutTsvArray) do
        local boardItem = {}
        -- Parse fields
        boardItem.columnIndex = tonumber(lineTsv[1])
        boardItem.name = lineTsv[2]
        boardItem.bgColor = tonumber(lineTsv[3])
        if boardItem.columnIndex == nil then
            log(2, 'Ignoring pool layout line ' .. (lineNumber or 'nil') .. ' (bad columnIndex)')
            boardItem = nil
        end
        if type(boardItem.name) ~= 'string' or boardItem.name == '' then
            log(2, 'Ignoring pool layout line ' .. (lineNumber or 'nil') .. ' (invalid name)', boardItem.name)
            boardItem = nil
        end
        if type(lineTsv[3]) == 'string' and boardItem.bgColor == nil then
            log(3, 'Unable to parse pool layout line ' .. (lineNumber or 'nil') .. ' background color', lineTsv[3])
        end

        -- Add to lookup and check for name collision
        if boardItem ~= nil then
            boardNameLookup = boardNameLookup or {}
            if boardNameLookup[boardItem.name] == nil then
                boardNameLookup[boardItem.name] = boardItem
            else
                log(2, 'Ignoring pool layout line ' .. (lineNumber or 'nil') .. ' (duplicated name)', boardItem.name)
                boardItem = nil
            end
        end

        -- Add this board if parsing went well
        if boardItem ~= nil then
            boardArray = boardArray or {}
            table.insert(boardArray, boardItem)
        end
    end

    return true
end

-- #### Dive list function ####

local diveArray -- Array of {serie, subSerie, orderIndex, boardName, diverName, diveNumber, diveName}
local diverNameArray -- array of all the 'individual' diveArray[].diverName
local diverDiveArray -- array for each [diverName] of all the diveItem for this diver
function diveProgram_printDives()
    if diveArray == nil then
        log(2, 'No dive list loaded')
        return false
    end

    local startOrder = 1
    local previousSerie = nil
    local previousOrderIndex = nil
    print(version)
    for _,diveItem in ipairs(diveArray) do
        if previousSerie ~= diveItem.serie then
            startOrder = 1
            previousOrderIndex = nil
            previousSerie = diveItem.serie
            print("Série " .. (diveItem.serie or 'nil') .. ':')
        end
        if diveItem.orderIndex ~= nil and previousOrderIndex == diveItem.orderIndex then
            startOrder = startOrder - 1 -- UGLY
        else
            previousOrderIndex = diveItem.orderIndex
        end
        local serieStr = (diveItem.serie or '') .. (diveItem.subSerie or '')
        print('' .. string.format('%2d',startOrder) .. '.  ' ..
            serieStr,
            diveItem.boardName, diveItem.diverName, diveItem.diveNumber, diveItem.diveName)
        startOrder = startOrder + 1
    end
end

local function diveProgram_consolidateChronology()
    if diveArray == nil then
        log(2, 'No dive list loaded')
        return false
    end

    -- Enrich optional fields

    -- First, ensure the all have a subSerie (autobuild based on columns)
    -- TODO, assume this is true for now
    -- for _,diveItem in ipairs(diveArray) do
    -- end

    -- Then, ensure they all have an orderIndex
    -- (autobuild based on previous, gap will be resolved later)
    local previousSerie = nil
    local previousOrderIndex = 0
    local previousOrderIndexWasExplicited = nil
    for _,diveItem in ipairs(diveArray) do
        -- Force new serie to start from index 1
        if previousSerie ~= diveItem.serie then
            if diveItem.orderIndex ~= nil and diveItem.orderIndex ~= 1 then
                log(3, 'First dive in serie is explicit but in not 1, overriding (serie, subSerie, orderIndex, boardName, diveNumber)',
                    diveItem.serie, diveItem.subSerie, diveItem.orderIndex, diveItem.boardName, diveItem.diveNumber)
            end
            previousOrderIndex = 0
            previousOrderIndexWasExplicited = nil
        end

        if diveItem.orderIndex == nil then
            diveItem.orderIndex = previousOrderIndex + 1
            previousOrderIndexWasExplicited = false
        else
            if previousOrderIndexWasExplicited == true and diveItem.orderIndex < previousOrderIndex then -- equal means synchro
                log(2, 'Conflict between current dive orderIndex and previous (explicit) one (serie, subSerie, orderIndex, boardName, diveNumber)',
                    diveItem.serie, diveItem.subSerie, diveItem.orderIndex, diveItem.boardName, diveItem.diveNumber)
                return false
            elseif previousOrderIndexWasExplicited == false and diveItem.orderIndex <= previousOrderIndex then
                log(2, 'Conflict between current dive orderIndex and previous (autobuilt) one (serie, subSerie, orderIndex, boardName, diveNumber)',
                    diveItem.serie, diveItem.subSerie, diveItem.orderIndex, diveItem.boardName, diveItem.diveNumber)
                return false
            end
            previousOrderIndexWasExplicited = true
        end
        previousOrderIndex = diveItem.orderIndex
        previousSerie = diveItem.serie
    end

    -- Normalize orderIndex (remove gaps)
    local oldToNewOrderIndex = {}
    local previousSerie = nil
    local previousOldOrderIndex = 0
    local previousNewOrderIndex = 0
    for _,diveItem in ipairs(diveArray) do
        local oldOrderIndex, newOrderIndex
        if previousSerie ~= diveItem.serie then
            previousOldOrderIndex = 0
            previousNewOrderIndex = 0
        end
        oldOrderIndex = diveItem.orderIndex

        if oldOrderIndex == previousOldOrderIndex then -- synchro
            newOrderIndex = previousNewOrderIndex
        elseif oldOrderIndex > previousOldOrderIndex then
            newOrderIndex = previousNewOrderIndex + 1 -- Fix possible gaps
        else
            log(2, 'Inconsistent dive orderIndex, this should not happen (serie, subSerie, orderIndex, boardName, diveNumber)',
                    diveItem.serie, diveItem.subSerie, diveItem.orderIndex, diveItem.boardName, diveItem.diveNumber)
            return false
        end

        -- Apply new value
        diveItem.orderIndex = newOrderIndex

        previousOldOrderIndex = oldOrderIndex
        previousNewOrderIndex = newOrderIndex
        previousSerie = diveItem.serie
    end

    return true
end

local function diveProgram_rebuildDiverNameArray()
    if diveArray == nil then
        log(2, 'No dive list loaded')
        return false
    end

    -- Clear previous
    diverNameArray = nil

    -- Iterate on the dive list, extract the names
    local diveNameLookup = {}
    for _,diveItem in ipairs(diveArray) do
        -- Handle synchro dives 'Diver A + diver B + Diver C'
        local diveItemNames = split(diveItem.diverName,'+', false)
        for _,untrimmedName in ipairs(diveItemNames or {}) do
            if type(untrimmedName) == 'string' then
                -- Trim leading and training whitespace
                local name = string.gsub(untrimmedName, "^%s*(.-)%s*$", "%1")
                if type(name) == 'string' and name ~= '' then
                    diveNameLookup[name] = true
                end
            end
        end
    end
    for name,_ in pairs(diveNameLookup) do
        diverNameArray = diverNameArray or {}
        table.insert(diverNameArray, name)
    end
    if diverNameArray == nil then
        log(2, 'No diver names found')
        return false
    end

    -- sort alphabetically
    table.sort(diverNameArray)

    return true
end

local function diveProgram_rebuildDiverDiveArray()
    if diveArray == nil or diverNameArray == nil then
        log(2, 'No dive list loaded or no diver name array')
        return false
    end

    -- Clear previous
    diverDiveArray = nil

    -- Iterate on the diver name list
    for _,diverName in ipairs(diverNameArray) do
        diverDiveArray = diverDiveArray or {}
        diverDiveArray[diverName] = {}

        -- Iterate on the dive list, find this diver dives
        for _,diveItem in ipairs(diveArray) do
            if type(diveItem.diverName) == 'string' and diveItem.diverName:find(diverName) ~= nil then
                table.insert(diverDiveArray[diverName], diveItem)
            end
        end
    end

    return true
end

function diveProgram_loadDives(diveListFilePath)
    if boardNameLookup == nil then
        log(2, 'No boards in pool layout')
        return false
    end

    -- Clear previous
    diveArray = nil
    diverNameArray = nil

    -- Load dive tsv
    local diveTsvArray = files_tsvToArray(diveListFilePath)
    if diveTsvArray == nil then
        log(2, 'Unable to load dive list', diveListFilePath)
        return false
    end

    -- Parse tsv
    for lineNumber, diveTsv in ipairs(diveTsvArray) do
        local diveItem = {}
        -- Parse fields
        diveItem.serie = diveTsv[1]
        diveItem.subSerie = tonumber(diveTsv[2])
        diveItem.orderIndex = tonumber(diveTsv[3])
        diveItem.boardName = diveTsv[4]
        diveItem.diverName = diveTsv[5]
        diveItem.diveNumber = diveTsv[6]
        diveItem.diveName = diveTsv[7]

        -- Check mandatory fields
        if diveItem ~= nil and (type(diveItem.boardName) ~= 'string' or diveItem.boardName == '' or boardNameLookup[diveItem.boardName] == nil) then
            log(2, 'Ignoring dive list line ' .. (lineNumber or 'nil') .. ' (invalid board name)', diveItem.boardName)
            diveItem = nil
        end
        if diveItem ~= nil and (type(diveItem.diverName) ~= 'string' or diveItem.diverName == '') then
            log(2, 'Ignoring dive list line ' .. (lineNumber or 'nil') .. ' (invalid diver name)', diveItem.diverName)
            diveItem = nil
        end
        -- either need a dive number or a dive name
        if diveItem ~= nil and ((type(diveItem.diveNumber) ~= 'string' or diveItem.diveNumber == '') and (type(diveItem.diveName) ~= 'string' or diveItem.diveName == '')) then
            log(2, 'Ignoring dive list line ' .. (lineNumber or 'nil') .. ' (no diveNumber nor diveName)', diveItem.diveNumber, diveItem.diveName)
            diveItem = nil
        end

        -- Add this dive if parsing went well
        if diveItem ~= nil then
            diveArray = diveArray or {}
            table.insert(diveArray, diveItem)
        end
    end

    -- Fill optional chronology field (subSeries, orderIndex)
    if diveProgram_consolidateChronology() ~= true then
        log(2, 'Unable to consolidate chronology')
        return false
    end

    -- Fetch DiveName if only diveNumber was provided
    for _,diveItem in ipairs(diveArray) do
        if diveItem.diveName == nil and type(diveNumberToDiveName) == 'table' then
            if type(diveItem.diveNumber) == 'string' and type(diveNumberToDiveName[diveItem.diveNumber]) == 'string' then
                diveItem.diveName = diveNumberToDiveName[diveItem.diveNumber]
            end
        end
    end

    -- Build lookup and arrays
    if diveProgram_rebuildDiverNameArray() ~= true then
        log(2, 'Unable to rebuild diver name array')
        return false
    end
    if diveProgram_rebuildDiverDiveArray() ~= true then
        log(2, 'Unable to rebuild diver dive array')
        return false
    end

    return true
end

function diveProgram_saveProgramHtml(programHtmlFilePage)
    -- Fail if no pool layout or dive list
    if boardNameLookup == nil or boardNameLookup == nil then
        log(2, 'No boards in pool layout')
        return false
    end
    if diveArray == nil or diverNameArray == nil then
        log(2, 'No dive list')
        return false
    end

    local css = files_fileToStr("programCssReference.css")

    local autoRefreshPeriodHtml = ''
    if autoRefreshPeriod ~= nil then
        autoRefreshPeriodHtml = '<meta http-equiv="refresh" content="' .. autoRefreshPeriod ..'" />'
    end

    local htmlHeader='<!DOCTYPE html><html lang="fr"><head><meta charset="utf-8"/><title>Programme ' .. (galaName or 'nil') ..'</title>' ..
        autoRefreshPeriodHtml ..
        '<style>'..css..'</style>' ..
        '</head><body>'
    local htmlFooter='</body></html>'

    -- Program for the whole gala
    local htmlBody = '<h1>Programme ' .. (galaName or 'nil') .. '</h1>'

    -- Autobuild arrays to know what to display -- TODO temporary, need a better autobuild before
    local serieLookup
    local subSeries2dLookup
    for i,diveItem in ipairs(diveArray) do
        if diveItem.serie ~= nil then
            serieLookup = serieLookup or {}
            serieLookup[diveItem.serie] = true
            if diveItem.subSerie ~= nil then
                subSeries2dLookup = subSeries2dLookup or {}
                subSeries2dLookup[diveItem.serie] = subSeries2dLookup[diveItem.serie] or {}
                subSeries2dLookup[diveItem.serie][diveItem.subSerie] = true
            end
        end
    end
    local serieArray
    local subSeries2dArray

    for i,_ in pairs(serieLookup) do
        serieArray = serieArray or {}
        table.insert(serieArray, i)
        for j,_ in pairs(subSeries2dLookup[i]) do
            subSeries2dArray = subSeries2dArray or {}
            subSeries2dArray[i] = subSeries2dArray[i] or {}
            table.insert(subSeries2dArray[i], j)
        end
    end
    table.sort(serieArray)
    for i,v in pairs(subSeries2dArray) do
        table.sort(v)
    end

    -- Build a page per serie
    for _,serie in ipairs(serieArray) do
        local serieHtml = '<div class="serie">'
        serieHtml = serieHtml .. '<div class="serieName">Série ' .. serie ..'</div>'

        -- Build a table per subSerie
        for _,subSerie in ipairs(subSeries2dArray[serie] or {}) do
            local subSerieHtml = '<div class="subSerie">'
            subSerieHtml = subSerieHtml .. '<div class="subSerieName">' .. serie .. subSerie .. '</div>'

            -- Build the layout for this subserie
            local subSerieColumnCount = 0
            for i,boardItem in ipairs(boardArray) do
                if subSerieColumnCount < boardItem.columnIndex then
                    subSerieColumnCount = boardItem.columnIndex
                end
            end

            -- Iterate on the dive list, extract the dives for this subseries
            local subSerieColumnArray = {}
            local subSerieSerieIndexArray = {} -- To detect synchro accross boards
            for i,diveItem in ipairs(diveArray) do
                if diveItem.serie == serie and diveItem.subSerie == subSerie then
                    local diveColumn = boardNameLookup[diveItem.boardName].columnIndex -- TODO protect against nil
                    if diveColumn == nil or diveColumn > subSerieColumnCount then
                        log(2, 'No valid column for dive, dropping it (diveColumn, serie, subSerie, boardName, diveNumber)', diveColumn, serie, subSerie, diveItem.boardName, diveItem.diveNumber)
                    else
                        if subSerieColumnArray[diveColumn] ~= nil then
                            log(2, 'Warning, multiple dives on the same column (serie, subSerie, column)', serie, subSerie, diveColumn)
                            -- TODO, add more debug on who are the two dives fighting for the same spot
                        end

                        local subSerieColumnEntry = {}
                        subSerieColumnEntry.boardName = diveItem.boardName
                        subSerieColumnEntry.serieIndex = diveItem.orderIndex
                        subSerieColumnEntry.diverName = diveItem.diverName
                        subSerieColumnEntry.diveNumber = diveItem.diveNumber
                        subSerieColumnEntry.diveName = diveItem.diveName
                        subSerieColumnArray[diveColumn] = subSerieColumnEntry
                        subSerieSerieIndexArray[diveColumn] = subSerieColumnEntry.serieIndex
                    end
                end
            end

            -- Build a cell per column
            for columnIndex=1,subSerieColumnCount do
                local subSerieColumnEntry = subSerieColumnArray[columnIndex]
                local columnHtml = ''

                -- Detect cells that share a orderIndex (or an emptiness) with their neighbour
                local synchroExtraClass = ''
                if columnIndex > 1 and subSerieSerieIndexArray[columnIndex-1] == subSerieSerieIndexArray[columnIndex] then
                    synchroExtraClass = synchroExtraClass .. ' ' .. 'boardSynchroWithLeft'
                end
                if columnIndex < subSerieColumnCount and subSerieSerieIndexArray[columnIndex+1] == subSerieSerieIndexArray[columnIndex] then
                    synchroExtraClass = synchroExtraClass .. ' ' .. 'boardSynchroWithRight'
                end

                columnHtml = columnHtml .. '\n<div class="board' .. (synchroExtraClass or '') ..'">'
                if subSerieColumnEntry ~= nil then
                    local boardNameCss=''
                    if type(boardNameLookup[subSerieColumnEntry.boardName].bgColor) then
                        boardNameCss='background-color:'.. string.format('#%06x', boardNameLookup[subSerieColumnEntry.boardName].bgColor) ..';'
                    end
                    columnHtml = columnHtml .. '<div class="boardName" style="'..boardNameCss..'">' .. subSerieColumnEntry.boardName .. '</div>'
                    columnHtml = columnHtml .. '<div class="dive">'
                    columnHtml = columnHtml .. '<div class="divers">' .. subSerieColumnEntry.diverName .. '</div>'
                    if subSerieColumnEntry.diveNumber ~= nil then
                        columnHtml = columnHtml .. '<div class="diveNumber">' .. subSerieColumnEntry.diveNumber .. '</div>'
                    end
                    if subSerieColumnEntry.diveName ~= nil then
                        columnHtml = columnHtml .. '<div class="diveName">' .. subSerieColumnEntry.diveName .. '</div>'
                    end
                    columnHtml = columnHtml .. '</div>' -- dive class
                    if subSerieColumnEntry.serieIndex ~= nil then
                        columnHtml = columnHtml .. '<div class="diveIndex">' .. subSerieColumnEntry.serieIndex .. '</div>'
                    end
                end
                columnHtml = columnHtml .. '</div>' -- board class

                subSerieHtml = subSerieHtml .. columnHtml
            end

            subSerieHtml = subSerieHtml .. '</div>'
            serieHtml = serieHtml .. subSerieHtml
        end

        serieHtml = serieHtml .. '</div>'
        htmlBody = htmlBody .. serieHtml
    end

    -- Complete dive list
    htmlBody = htmlBody .. '<h1 class="PageBreakBefore">Listing complet</h1>'

    local fullListingArray = {} -- Array of {entryText, orderIndex, diverName, serie}
    local fullListingHtml = '<div class="listingGroup">' .. '<div class="listing">'
    local serieArray = {}
    fullListingHtml = fullListingHtml .. '<div class="listingName">' .. 'Tous les plongeurs' .. '</div>'
    local previousSerie = nil
    for _,diveItem in ipairs(diveArray) do
        -- Quick and dirty way to separate series
        if previousSerie ~= diveItem.serie then
            if previousSerie ~= nil then
                fullListingHtml = fullListingHtml .. '</div>' -- listingSerie
            end
            fullListingHtml = fullListingHtml .. '<div class="listingSerie"><div class="listingSerieName">' .. 'Série ' .. diveItem.serie ..'</div>'
            previousSerie = diveItem.serie
            table.insert(serieArray, diveItem.serie)
        end

        local listingEntryText = ''
        if diveItem.orderIndex ~= nil then
            listingEntryText = listingEntryText .. diveItem.orderIndex .. '. '
        end
        listingEntryText = listingEntryText .. (diveItem.serie or '') .. (diveItem.subSerie or '') .. ', '

        listingEntryText = listingEntryText .. (diveItem.boardName or '') .. ', '
        listingEntryText = listingEntryText .. '<strong>'..(diveItem.diverName or '') .. '</strong>'.. ', '

        listingEntryText = listingEntryText .. (diveItem.diveNumber or '') .. ' '
        listingEntryText = listingEntryText .. (diveItem.diveName or '')

        table.insert(fullListingArray, {entryText=listingEntryText, orderIndex=diveItem.orderIndex, diverName=diveItem.diverName, serie=diveItem.serie})
        fullListingHtml = fullListingHtml .. '<div class="listingEntry">' .. listingEntryText ..'</div>'
    end
    fullListingHtml = fullListingHtml .. '</div>' -- listingSerie
    fullListingHtml = fullListingHtml .. '</div>' -- listing
    fullListingHtml = fullListingHtml .. '</div>' -- listingGroup
    htmlBody = htmlBody .. fullListingHtml

    -- Individual dive list
    htmlBody = htmlBody .. '<h1 class="PageBreakBefore">Listing par plongeur</h1>'

    local allIndividualListingHtml = '<div class="listingGroup">'
    for _,diverName in ipairs(diverNameArray) do
        local currentIndividualListingHtml = '<div class="listing">'
        currentIndividualListingHtml = currentIndividualListingHtml .. '<div class="listingName">' .. diverName .. '</div>'
        for _,serie in ipairs(serieArray) do

            -- Tally the orderIndex for this diver in this serie
            local orderIndexLookup
            local serieLastOrderIndex = nil
            for _,diveItem in ipairs(diveArray) do
                local orderIndex = diveItem.orderIndex
                -- If this diver is involved
                if type(orderIndex) == 'number' then
                    if type(diveItem.diverName) == 'string' and diveItem.serie == serie and diveItem.diverName:find(diverName) ~= nil then
                        orderIndexLookup = orderIndexLookup or {}
                        orderIndexLookup[orderIndex] = true
                    end
                end
                serieLastOrderIndex = math.max(serieLastOrderIndex or orderIndex, orderIndex)
            end

            -- List the previous, the current and the next for each of his dives, if there's any in this serie
            if orderIndexLookup ~= nil then
                currentIndividualListingHtml = currentIndividualListingHtml .. '<div class="listingSerie"><div class="listingSerieName">' .. 'Série ' .. serie ..'</div>'
                local previousEntryIsTripleDot = false
                for i,diveItem in ipairs(diveArray) do
                    local orderIndex = diveItem.orderIndex
                    if type(orderIndex) == 'number' and diveItem.serie == serie then
                        if orderIndexLookup[orderIndex] == true then -- current
                            currentIndividualListingHtml = currentIndividualListingHtml .. '<div class="listingEntry">' .. fullListingArray[i].entryText ..'</div>'
                            previousEntryIsTripleDot = false
                        elseif orderIndexLookup[orderIndex] ~= true and orderIndexLookup[orderIndex+1] == true then -- previous
                            currentIndividualListingHtml = currentIndividualListingHtml .. '<div class="listingEntry faded">' .. fullListingArray[i].entryText ..'</div>'
                            previousEntryIsTripleDot = false
                        elseif orderIndexLookup[orderIndex] ~= true and orderIndexLookup[orderIndex-1] == true then -- next
                            currentIndividualListingHtml = currentIndividualListingHtml .. '<div class="listingEntry faded">' .. fullListingArray[i].entryText ..'</div>'
                            previousEntryIsTripleDot = false
                        elseif (orderIndexLookup[orderIndex] ~= true and orderIndexLookup[orderIndex+1] ~= true and orderIndexLookup[orderIndex+2] == true) -- preprevious
                            or (orderIndexLookup[orderIndex] ~= true and orderIndexLookup[orderIndex-1] ~= true and orderIndexLookup[orderIndex-2] == true) then -- nenext
                            if previousEntryIsTripleDot == false then
                                -- Count how many dives between this and the next
                                local previousOrderIndex, nextOrderIndex
                                for index,value in pairs(orderIndexLookup) do
                                    if index < orderIndex then
                                        if previousOrderIndex == nil or previousOrderIndex < index then
                                            previousOrderIndex = index
                                        end
                                    end
                                    if index > orderIndex then
                                        if nextOrderIndex == nil or nextOrderIndex > index then
                                            nextOrderIndex = index
                                        end
                                    end
                                end
                                local ommitedDiveCount = nil
                                if previousOrderIndex ~= nil and nextOrderIndex ~= nil then -- Middle ...
                                    ommitedDiveCount = nextOrderIndex - previousOrderIndex - 2
                                elseif previousOrderIndex ~= nil and nextOrderIndex == nil then -- last
                                    ommitedDiveCount = serieLastOrderIndex - previousOrderIndex - 1
                                elseif previousOrderIndex == nil and nextOrderIndex ~= nil then -- last
                                    ommitedDiveCount = nextOrderIndex - 1 - 1
                                else
                                    log(2, 'Invalid internal error, ... must be either first, last or inbetween')
                                end
                                local pluralStr = ''
                                if ommitedDiveCount > 1 then pluralStr = 's' end
                                currentIndividualListingHtml = currentIndividualListingHtml .. '<div class="listingEntry faded centered">... '..
                                    (ommitedDiveCount or 'nil') ..' plongeon'..(pluralStr or '')..' ...</div>'
                                previousEntryIsTripleDot = true
                            end
                        end
                    end
                end
                currentIndividualListingHtml = currentIndividualListingHtml .. '</div>' -- listingSerie
            end

        end
        currentIndividualListingHtml = currentIndividualListingHtml ..  '</div>' -- listing
        allIndividualListingHtml = allIndividualListingHtml .. currentIndividualListingHtml
    end
    allIndividualListingHtml = allIndividualListingHtml .. '</div>' -- listingGroup
    htmlBody = htmlBody .. allIndividualListingHtml


    -- Dive allocation accross series and subseries (TODO, use div and css?)
    htmlBody = htmlBody .. '<h1 class="PageBreakBefore">Allocation des plongeurs</h1>'
    local allocationHtml = '<div class="allocations"><table border="1">'
    -- headers
    allocationHtml = allocationHtml .. '<tr>'
    allocationHtml = allocationHtml .. '<th>' .. 'Plongeur' .. '</th>'
    local diverDivesOnThisSerieSubSerie = {}
    for _,serie in pairs(serieArray) do
        for subSerie,_ in pairs(subSeries2dArray[serie]) do
            local concatenationOfSerieSubSerie = (serie or '') .. (subSerie or '')
            diverDivesOnThisSerieSubSerie[concatenationOfSerieSubSerie] = false -- Init lookup
            allocationHtml = allocationHtml .. '<th>' .. concatenationOfSerieSubSerie .. '</th>'
        end
    end
    allocationHtml = allocationHtml .. '</tr>'

    for _,diverName in ipairs(diverNameArray) do
        -- Clear lookup
        for i,_ in pairs(diverDivesOnThisSerieSubSerie) do
            diverDivesOnThisSerieSubSerie[i] = false
        end

        for _,diveItem in ipairs(diverDiveArray[diverName] or {}) do
            diverDivesOnThisSerieSubSerie[(diveItem.serie or '') .. (diveItem.subSerie or '')] = true
        end

        -- Checkmarks
        allocationHtml = allocationHtml .. '<tr>'
        allocationHtml = allocationHtml .. '<td>' .. (diverName or '') .. '</td>'
        for _,serie in pairs(serieArray) do
            for subSerie,_ in pairs(subSeries2dArray[serie]) do
                local concatenationOfSerieSubSerie = (serie or '') .. (subSerie or '')
                allocationHtml = allocationHtml .. '<td>'
                if diverDivesOnThisSerieSubSerie[concatenationOfSerieSubSerie] == true then
                    allocationHtml = allocationHtml .. 'X'
                end
                allocationHtml = allocationHtml .. '</td>'
            end
        end
        allocationHtml = allocationHtml .. '</tr>'
    end
    allocationHtml = allocationHtml .. '<table></div>'
    htmlBody = htmlBody .. allocationHtml


    -- Footer
    htmlBody = htmlBody .. '<footer>' .. (version or '') .. '</footer>'
    local htmlStr = htmlHeader .. htmlBody .. htmlFooter
    files_strToFile(htmlStr, programHtmlFilePage)

    return true
end
