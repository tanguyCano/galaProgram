#!/bin/sh

# Move to script folder
cd `dirname $0`
BASE_DIR=$PWD

LUA_BIN=$BASE_DIR/luaBinaries/mac/lua53
SCRIPT_FILE=buildProgram.lua
SRC_DIR=$BASE_DIR/src/

#Change these to your pool file, gala files and values
OUTPUT_FILE=$BASE_DIR/programme_TRN22.html
POOL_FILE=$BASE_DIR/pools/FranceRennes_Brequigny.tsv
GALA_FILE=$BASE_DIR/galas/2022_04_02_TRN.tsv
GALA_NAME="Gala Tout Rennes Nage 2022"
GALA_VERSION=6
#Leave empty to disable, set decimal value to enable
HTML_AUTO_REFRESH_PERIOD=

cd $SRC_DIR
$LUA_BIN $SCRIPT_FILE $POOL_FILE $GALA_FILE "$GALA_NAME" $GALA_VERSION $OUTPUT_FILE $HTML_AUTO_REFRESH_PERIOD
